<?php

use Latte\Runtime as LR;

/** source: template/template1.latte */
final class Template2c921fc442 extends Latte\Runtime\Template
{
	public const Source = 'template/template1.latte';


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ukázka template</title>
</head>
<body>
   

    <h2>Výpis PHP proměnné </h2>
        <p>
            ';
		echo LR\Filters::escapeHtmlText($ovoce) /* line 13 */;
		echo '
        </p>
    

    <h2>Výpis pole hodnot</h2>
        <p>
';
		foreach ($items1 as $items) /* line 19 */ {
			echo '                ';
			echo LR\Filters::escapeHtmlText($items) /* line 20 */;
			echo "\n";

		}

		echo '        </p>
    

    <h2>Výpis pole hodnot pomocí n:atribut;</h2>
';
		if ($items1) /* line 26 */ {
			echo '        <ul>
';
			foreach ($items1 as $item) /* line 27 */ {
				echo '	        <li>';
				echo LR\Filters::escapeHtmlText(($this->filters->capitalize)($item)) /* line 27 */;
				echo '</li>
';

			}

			echo '        </ul>
';
		}
		echo '    

    <h2>Použití podmínek - výpis hodnoty "two" barevně</h2>
        <p style="color: red;">
';
		foreach ($items1 as $items) /* line 33 */ {
			if ($items == 'two') /* line 34 */ {
				echo '                    ';
				echo LR\Filters::escapeHtmlText($items) /* line 35 */;
				echo "\n";
			}

		}

		echo '        </p>

    <h2>Podmínky</h2>
    <p>Úkol: Vypište dle hodnoty proměnné $ovoce alkoholický nápoj, který se z něj vyrábí (jablko,švestka,hruška)</p>
        <p>
';
		if ($ovoce == 'jablko') /* line 43 */ {
			echo '                jablkovice
';
		}
		if ($ovoce == 'svestka') /* line 46 */ {
			echo '                svestkovice
';
		}
		if ($ovoce == 'hruska') /* line 49 */ {
			echo '                hruskovice
';
		}
		echo '        </p>

    <h2>Ternární operátor</h2>
    <p>Úkol: Vypište dle hodnoty proměnné $rezervace větu Rezervováno nebo Volné k pronájmu</p>
        <p>
';
		if ($rezervace) /* line 57 */ {
			echo '                Rezervovano.
';
		} else /* line 59 */ {
			echo '                Volne k pronajmu
';
		}
		echo '        </p>
  

    <h2>Cyklus Foreach</h2>
    <p>Úkol: Vypište pole $objednávka jako HTML seznam</p>
';
		if ($objednavka) /* line 67 */ {
			echo '        <ul>
';
			foreach ($objednavka as $item) /* line 68 */ {
				echo '	        <li>';
				echo LR\Filters::escapeHtmlText(($this->filters->capitalize)($item)) /* line 68 */;
				echo '</li>
';

			}

			echo '        </ul>
';
		}
		echo '
    <h2>Cyklus For</h2>
    <p>Úkol: Vypište první tři položky pole $objednávka jako HTML seznam (formát výpisu - 1. položka 2.položka atd.)</p>
        <ul>
';
		for ($i = 1;
		$i < 4;
		$i++) /* line 74 */ {
			echo '                <li>';
			echo LR\Filters::escapeHtmlText($i) /* line 75 */;
			echo '. ';
			echo LR\Filters::escapeHtmlText($objednavka[$i]) /* line 75 */;
			echo '</li>
';

		}
		echo '        </ul>
    
    <h2>Bloky</h2>
    <p>Úkol: Vytvořte blok HTML kódu a použijte na jiném místě šablony</p>
';
		$this->createTemplate('index.html', $this->params, 'include')->renderToContentType('html') /* line 81 */;
		echo '    
</body>
</html>';
	}


	public function prepare(): array
	{
		extract($this->params);

		if (!$this->getReferringTemplate() || $this->getReferenceType() === 'extends') {
			foreach (array_intersect_key(['items' => '19, 33', 'item' => '27, 68'], $this->params) as $ʟ_v => $ʟ_l) {
				trigger_error("Variable \$$ʟ_v overwritten in foreach on line $ʟ_l");
			}
		}
		return get_defined_vars();
	}
}
